<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2011 - 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

JFactory::getDocument()->addStyleSheet(JURI::base(true) . '/modules/' . $module->module . '/tmpl/stylesheet.css');

if ($params->get('modtitle_overwrite') == 'yes' && $ts3->tree_head()) {
    $module->title = '<a href="ts3server://' . $params->get('ip') . '?port=' . $params->get('udp') . '" class="ts3colink">' . $ts3->tree_head() . '</a>';
}
?>

<div class="ts3viewer<?php echo $params->get('moduleclass_sfx'); ?>">
    <?php echo $ts3->tree(); ?>

    <?php if ($params->get('layout_stats') == 'yes') { ?>
        <br/>
        <?php echo $ts3->stats(implode(', ', $params->get('layout_stats_hide', array()))); ?>
    <?php } ?>
</div>