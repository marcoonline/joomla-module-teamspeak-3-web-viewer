<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2011 - 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

abstract class mod_mh_ts3viewer
{

    public static function onload(JRegistry &$params, stdClass &$module)
    {

        include_once dirname(__FILE__) . '/class.ts3viewer.php';

        $ts3 = new TS3Viewer($params->get('ip'), $params->get('query'), $params->get('sid'), $params->get('udp'));

        $ts3->cache_handler = $params->get('ts3cache', 'file');
        $ts3->cache_timeout = $params->get('ts3cachetime', 60);
        $ts3->cache_path = JPATH_CACHE . '/';

        $ts3->socket_timeout = $params->get('connection_timeout', $ts3->socket_timeout);

        ($params->get('wildcard') == 'no') ? $ts3->wildcard = null : true;

        $ts3->img_path = $params->get('img_path', JURI::base(true) . '/modules/' . $module->module . '/tmpl/images/');
        $ts3->spacer_multi = $params->get('spacer_multi', 50);

        if ($params->get('group_sadmin')) {
            unset($ts3->groups['sgroup'][6]);
            $ts3->groups['sgroup'][$params->get('group_sadmin')] = array('n' => 'Server Admin', 'p' => 'client_sa.png');
        }

        if ($params->get('group_cadmin')) {
            unset($ts3->groups['cgroup'][5]);
            $ts3->groups['cgroup'][$params->get('group_cadmin')] = array('n' => 'Channel Admin', 'p' => 'client_ca.png');
        }

        if ($params->get('group_coperator')) {
            unset($ts3->groups['cgroup'][6]);
            $ts3->groups['cgroup'][$params->get('group_coperator')] = array('n' => 'Channel Operator', 'p' => 'client_co.png');
        }

        if ($params->get('group_member')) {
            $ts3->groups['sgroup'][$params->get('group_member')] = array('n' => 'Member', 'p' => 'member.png');
        }

        if ($params->get('group_guest')) {
            $ts3->groups['sgroup'][$params->get('group_guest')] = array('n' => 'Guest', 'p' => 'guest.png');
        }

        if ($params->get('icon_record') == 'yes') {
            $ts3->icons['client_is_recording'] = 1;
        }

        if ($params->get('icon_talking') == 'no') {
            unset($ts3->icons['client_flag_talking']);
        }

        if ($params->get('channel_hide')) {
            $channel_hide = explode(';', $params->get('channel_hide'));
            $channel_hide = array_map('strtolower', $channel_hide);
            $channel_hide = array_map('trim', $channel_hide);
            $channel_hide = array_filter($channel_hide);
            $ts3->chide = array_flip($channel_hide);
        }

        if ($params->get('server_login') && $params->get('server_password')) {
            $ts3->serverlogin = array('login' => $params->get('server_login'), 'password' => $params->get('server_password'));
        }

        $ts3->lang['stats']['os'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_OS');
        $ts3->lang['stats']['version'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_VERSION');
        $ts3->lang['stats']['channels'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_CHANNELS');
        $ts3->lang['stats']['onlinesince'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_ONLINE_SINCE');
        $ts3->lang['stats']['uptime'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_UPTIME');
        $ts3->lang['stats']['traffic'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_TRAFFIC');
        $ts3->lang['stats']['cache'] = JText::_('MOD_MH_TS3VIEWER_LANG_STATS_CACHE');

        $ts3->build();

        return $ts3;
    }
}